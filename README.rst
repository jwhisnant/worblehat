===============================
worblehat
===============================

| |docs| |travis| |appveyor| |coveralls| |landscape| |scrutinizer|
| |version| |downloads| |wheel| |supported-versions| |supported-implementations|

.. |docs| image:: https://readthedocs.org/projects/worblehat/badge/?style=flat
    :target: https://readthedocs.org/projects/worblehat
    :alt: Documentation Status

.. |travis| image:: http://img.shields.io/travis/jwhisnant/worblehat/master.png?style=flat
    :alt: Travis-CI Build Status
    :target: https://travis-ci.org/jwhisnant/worblehat

.. |appveyor| image:: https://ci.appveyor.com/api/projects/status/github/jwhisnant/worblehat?branch=master
    :alt: AppVeyor Build Status
    :target: https://ci.appveyor.com/project/jwhisnant/worblehat

.. |coveralls| image:: http://img.shields.io/coveralls/jwhisnant/worblehat/master.png?style=flat
    :alt: Coverage Status
    :target: https://coveralls.io/r/jwhisnant/worblehat

.. |landscape| image:: https://landscape.io/github/jwhisnant/worblehat/master/landscape.svg?style=flat
    :target: https://landscape.io/github/jwhisnant/worblehat/master
    :alt: Code Quality Status

.. |version| image:: http://img.shields.io/pypi/v/worblehat.png?style=flat
    :alt: PyPI Package latest release
    :target: https://pypi.python.org/pypi/worblehat

.. |downloads| image:: http://img.shields.io/pypi/dm/worblehat.png?style=flat
    :alt: PyPI Package monthly downloads
    :target: https://pypi.python.org/pypi/worblehat

.. |wheel| image:: https://pypip.in/wheel/worblehat/badge.png?style=flat
    :alt: PyPI Wheel
    :target: https://pypi.python.org/pypi/worblehat

.. |supported-versions| image:: https://pypip.in/py_versions/worblehat/badge.png?style=flat
    :alt: Supported versions
    :target: https://pypi.python.org/pypi/worblehat

.. |supported-implementations| image:: https://pypip.in/implementation/worblehat/badge.png?style=flat
    :alt: Supported imlementations
    :target: https://pypi.python.org/pypi/worblehat

.. |scrutinizer| image:: https://img.shields.io/scrutinizer/g/jwhisnant/worblehat/master.png?style=flat
    :alt: Scrutinizer Status
    :target: https://scrutinizer-ci.com/g/jwhisnant/worblehat/

Do not interfere with the nature of causality.

* Free software: BSD license

Installation
============

::

    pip install worblehat

Documentation
=============

https://worblehat.readthedocs.org/

Development
===========

To run the all tests run::

    tox
