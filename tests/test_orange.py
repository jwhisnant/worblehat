"""
Selenium webdriver to get PIN documents
from Orange County NC webpage

# XXX TODO: how to close firefox on error  2015/03/15  (james)
# XXX TODO: tests 2015/03/15  (james)
"""

import os

# import time
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.support import expected_conditions as EC

import logging
FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# pin_id_entry = "ctl00_SectionContents_ASPxCallbackPanel1_ASPxRoundPanel2_ASPxButtonEditPin_I"
# pin_search_id = "ctl00_SectionContents_ASPxCallbackPanel1_ASPxRoundPanel2_ASPxButtonEditPin_B0"
# doc_id = "ctl00_SectionContents_ASPxCallbackPanel1_ASPxRoundPanel2_ASPxTreeList1_tDC1_4_hyperLinkTree"
# pinid = "ctl00_SectionContents_ASPxCallbackPanel2_ASPxRoundPanel3_ASPxGridViewLookup_col0_I"
# click_box = "ctl00_SectionContents_ASPxCallbackPanel2_ASPxRoundPanel3_ASPxGridViewLookup_DXDataRow0"

# XXX FIXME: how do I get last of a type?? 2015/03/16  (james)

from worblehat.orange import Pin
import unittest
import pytest
import subprocess
import psutil


class OrangeTestCase(unittest.TestCase):

    def setUp(self):
        """
        """
        self.pin = Pin()

    @pytest.mark.run(order=1)
    def test_display_set(self):
        """TODO: Docstring for test_dis_env_set.
        :returns: TODO

        """
        display = os.getenv('DISPLAY', None) or None
        assert display is not None

    def _check_firefox(self):
        out = []
        for proc in psutil.process_iter():
            if 'firefox' in proc.name():
                out.append(proc)
        return out

    @pytest.mark.run(order=2)
    def test_web_open(self):
        """TODO: Docstring for test_web_open.
        :returns: TODO

        """
        proc = self._check_firefox()
        assert not proc
        self.pin.web_open()  # should open pid

        proc = self._check_firefox()
        assert len(proc)==1

        self.pid = proc[0].pid
        return self.pid

    @pytest.mark.run(order=3)
    def test_picture_die(self):
        """TODO: Docstring for test_picture_die.
        :returns: TODO
        """
        welcome_id = "ctl00_SectionContents_ASPxPopupControl2_HCB-1Img"
        assert welcome_id in 'static_html'
        self.pin.picture_die()

        # reload next html
        assert welcome_id not in 'static_html'

    @pytest.mark.run(order=4)
    def test_enter_pin(self, static_html=''):
        """Enter the PIN number to serarch for in the PIN search form
        and send a CR to get the form to search
        :returns: TODO
        """
        # load static html
        element = self.pin_test.enter_pin()
        assert 'dxeEditArea' in element.class_name

    @pytest.mark.run(order=5)
    def test_write_png(self):
        """
        we create a file
        and it is a png file
        """
        fqn, pgn = self.pin.write_png()
        assert os.path.isfile(fqn)
        # unix only
        # XXX FIXME: dont use popen 2015/03/18  (james)
        assert 'PNG image data' in os.popen('file %s' % (fqn)).read()

    @pytest.mark.run(order=5)
    def test_pin_history(self, adict=None):
        """
        should return a summary of the pin history tab
        in a well navigable format
        """
        if not adict:
            adict = {}
        assert sorted(adict) == sorted(['Parent', 'Children', 'Selected'])

    @pytest.mark.run(order=7)
    def clean_up(self):
        os.kill(self.pid)

    #@classmethod
    # def tearDownClass(cls):
        # os.kill(self.pid)

'''
class Pin(object):

    def __init__(self,
                 url='http://web.co.orange.nc.us/PINManagementWeb/PinInquiry.aspx',
                 pin='9748872017'
                 ):
                 # pin='9748861601'

        self.url = url
        self.pin = pin

        self.deed_mapping = {}

    def __call__(self):
        self.web_open()

        self.picture_die()
        self.enter_self()
        self.write_png()

        # wait to make sure out page has loaded
        self._wait_for_footer()

        time.sleep(5)  # XXX FIXME: use a wait for 2015/03/17  (james)

        parent, selected = self.find_parent_and_child_links()

        # wait for selected to be clickable and click
        self._click_selected(selected)

        # self.wait_for_deed_grid()
        self._find_dxgvTables()  # wait for the first one ...

        tables_found = set()
        while len(tables_found) < 3:
            tables_found = self.driver.find_elements_by_class_name('dxgvTable')

        time.sleep(2)
        summary, junk, docs = self.driver.find_elements_by_class_name('dxgvTable')

        self._decompose_summary(summary)

    def _summary(self):
        """
        prevent StaleExceptionErrors by running this
        to get the summary
        """
        wanted, junk, docs = self.driver.find_elements_by_class_name('dxgvTable')
        return wanted

    def _decompose_summary(self, summary):
        """
        return a list of words
        u'Status:',
        u'Active',
        u' ',
        u' ',
        u'Interest Owners Exist',
        u'PIN:',
        u'9748872017',
        """
        import ipdb; ipdb.set_trace()  # XXX BREAKPOINT

        all_elements = self._summary().find_elements_by_xpath('.//*')
        out = [x.text for x in all_elements][1:]
        return out

    def _click_selected(self, element):
        """
        wait for, and click on an element
        """

        wait = WebDriverWait(self.driver, 10)
        link = wait.until(EC.element_to_be_clickable((By.ID, element.get_attribute('id'))))
        link.click()

    def find_parent_and_child_links(self):
        """
        find clickable document links
        #XXX TODO: get relationship text 2015/03/17  (james)
        """

        out = []
        for elem in self.driver.find_elements_by_class_name('dxeHyperlink'):
            attrib = elem.get_attribute('href')
            if u'javascript:void' in attrib:
                out.append(elem)
        # assert len(out) == 2 # only true in parent child case #XXX FIXME:  2015/03/17  (james)
        parent, selected = out  # XXX FIXME: nope 2015/03/17  (james)
        return parent, selected

    def _find_dxgvTables(self):
        """
        assume there are three of these fields
        at least wait a while until the first loads
        """
        wait = WebDriverWait(self.driver, 10)
        # elems = wait.until(self._three_of_a_kind())
        table = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'dxgvControl')))
        return table

    def _wait_for_footer(self):
        """
        wait until page has loaded.  we will use the footer for this
        """
        wait = WebDriverWait(self.driver, 10)
        footer = wait.until(EC.visibility_of_element_located((By.ID, 'footer_right')))
        return footer

    def web_open(self):
        """open a web-broswer in selenium
        :returns: a selenium webdriver
        """

        self.driver = webdriver.Firefox()
        self.driver.set_window_size(1124, 850)  # set browser size.
        self.actionChains = ActionChains(self.driver)
        self.driver.get(self.url)
        return self.driver

    def _click(self, element):
        """convenience to click on an element
        :element: WebDriver element
        :returns: WebDriver element

        """
        self.actionChains.click(element).perform()
        return element

    def picture_die(self):
        """Close the welcome this webpage dialog
        :returns: welcome element
        """
        # XXX TODO: dont do by ugly ID 2015/03/18  (james)
        welcome_id = "ctl00_SectionContents_ASPxPopupControl2_HCB-1Img"
        welcome = self.driver.find_element_by_id(welcome_id)
        self._click(welcome)
        return welcome

    def enter_pin(self):
        """Enter the PIN number to serarch for in the PIN search form
        and send a CR to get the form to search
        :returns: TODO
        """
        element = self.driver.find_element_by_class_name('dxeEditArea')
        element.send_keys(self.pin)  # onkey event here

        # send CR
        element.send_keys('\n')  # onkey event here
        return element

    def write_png(self):
        """Write a png file to disk
        :returns: png image
        """

        fn = '%s_%s.pdf' % (self.pin, time.strftime('%Y-%m-%d-%H-%M'))
        fqn = os.path.join('/var/tmp', fn)
        png = self.driver.get_screenshot_as_file(fqn)
        msg = 'Wrote image to %s' % (fqn)
        logger.info(msg)
        return fqn,png

    def _find_docgroup(self, elements):
        """
        find the group that contains
        the document icon we want
        """
        out = []
        for num, elem in enumerate(elements):
            if self.pin in elem.text:
                out = elements[num+1]
                return out
        return out

if __name__ == '__main__':
    pin = Pin()
    pin()
'''
