"""
Selenium webdriver to get PIN documents
from Orange County NC webpage

# XXX TODO: how to close firefox on error  2015/03/15  (james)
# XXX TODO: tests 2015/03/15  (james)
"""

import time
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC

import logging
FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# pin_id_entry = "ctl00_SectionContents_ASPxCallbackPanel1_ASPxRoundPanel2_ASPxButtonEditPin_I"
# pin_search_id = "ctl00_SectionContents_ASPxCallbackPanel1_ASPxRoundPanel2_ASPxButtonEditPin_B0"
# doc_id = "ctl00_SectionContents_ASPxCallbackPanel1_ASPxRoundPanel2_ASPxTreeList1_tDC1_4_hyperLinkTree"
# pinid = "ctl00_SectionContents_ASPxCallbackPanel2_ASPxRoundPanel3_ASPxGridViewLookup_col0_I"
# click_box = "ctl00_SectionContents_ASPxCallbackPanel2_ASPxRoundPanel3_ASPxGridViewLookup_DXDataRow0"

# XXX FIXME: how do I get last of a type?? 2015/03/16  (james)


class Pin(object):
    def __init__(self,
                 url='http://web.co.orange.nc.us/PINManagementWeb/PinInquiry.aspx',
                 pin='9748872017',
                 display=':0',
                 ):
                 # pin='9748861601'
        self.url = url
        self.pin = pin
        self.display = display
        self.deed_mapping = {}
    def __call__(self):
        self.env_check()
        self._set_profile()
        self.web_open()
        self.picture_die()
        self.enter_pin()
        # capture the summary info
        self.write_png()
        # wait to make sure out page has loaded
        self._wait_for_footer()
        time.sleep(5)  # XXX FIXME: use a wait for 2015/03/17  (james)
        parent, selected = self.find_parent_and_child_links()
        # wait for selected to be clickable and click
        self._click_selected(selected)
        # self.wait_for_deed_grid()
        self._find_dxgvTables()  # wait for the first one ...
        tables_found = set()
        while len(tables_found) < 3:
            tables_found = self.driver.find_elements_by_class_name('dxgvTable')
        time.sleep(2)
        summary, junk, docs = self.driver.find_elements_by_class_name('dxgvTable')
        time.sleep(5)  # we need this to not get stale errors, or fix it ...
        # capture document list
        self.write_png()
        self._decompose_summary(summary)
    def env_check(self):
        display = os.getenv('DISPLAY')
        if not display:
            logger.critical('You have no DISPLAY set.  Setting it to %s' % (self.display))
            os.environ['DISPLAY'] = self.display
    def _summary(self):
        """
        prevent StaleExceptionErrors by running this
        to get the summary
        """
        wanted, junk, docs = self.driver.find_elements_by_class_name('dxgvTable')
        return wanted
    def _decompose_summary(self, summary):
        """
        return a list of words
        u'Status:',
        u'Active',
        u' ',
        u' ',
        u'Interest Owners Exist',
        u'PIN:',
        u'9748872017',
        """
        logger.info('Gathering summary, this could take a while ...')
        all_elements = self._summary().find_elements_by_xpath('.//*')
        # XXX FIXME: this takes a very long time, but seems to work ... 2015/09/04  (james)
        # XXX FIXME: someimes no longer available errors 2015/09/05  (james)
        out = [x.text for x in all_elements][1:]
        logger.info(out)
        return out
    def _click_selected(self, element):
        """
        wait for, and click on an element
        """
        wait = WebDriverWait(self.driver, 10)
        link = wait.until(EC.element_to_be_clickable((By.ID, element.get_attribute('id'))))
        link.click()
    def find_parent_and_child_links(self):
        """
        find clickable document links
        #XXX TODO: get relationship text 2015/03/17  (james)
        """
        out = []
        for elem in self.driver.find_elements_by_class_name('dxeHyperlink'):
            attrib = elem.get_attribute('href')
            if u'javascript:void' in attrib:
                out.append(elem)
        # assert len(out) == 2 # only true in parent child case #XXX FIXME:  2015/03/17  (james)
        parent, selected = out  # XXX FIXME: nope 2015/03/17  (james)
        return parent, selected
    def _find_dxgvTables(self):
        """
        assume there are three of these fields
        at least wait a while until the first loads
        """
        wait = WebDriverWait(self.driver, 10)
        # elems = wait.until(self._three_of_a_kind())
        table = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'dxgvControl')))
        return table
    def _wait_for_footer(self):
        """
        wait until page has loaded.  we will use the footer for this
        """
        wait = WebDriverWait(self.driver, 10)
        footer = wait.until(EC.visibility_of_element_located((By.ID, 'footer_right')))
        return footer
    def _set_profile(self):
        """https://stackoverflow.com/questions/34552552/selenium-firefox-webdriver-wont-load-a-blank-page-after-changing-firefox-prefer"""
        self.profile = webdriver.FirefoxProfile()
        # Tell the browser to start on a blank page
        self.profile.set_preference('browser.startup.page', 0)
        self.profile.set_preference("browser.startup.homepage_override.mstone", "ignore")
    def web_open(self):
        """open a web-broswer in selenium
        :returns: a selenium webdriver
        """
        self.driver = webdriver.Firefox(self.profile)
        self.driver.get("about:blank")
        self.driver.set_window_size(1124, 850)  # set browser size.
        self.actionChains = ActionChains(self.driver)
        self.driver.get(self.url)
        return self.driver
    def _click(self, element):
        """convenience to click on an element
        :element: WebDriver element
        :returns: WebDriver element
        """
        self.actionChains.click(element).perform()
        return element
    def picture_die(self):
        """Close the welcome this webpage dialog
        :returns: welcome element
        """
        # XXX TODO: dont do by ugly ID 2015/03/18  (james)
        welcome_id = "ctl00_SectionContents_ASPxPopupControl2_HCB-1Img"
        welcome = self.driver.find_element_by_id(welcome_id)
        self._click(welcome)
        return welcome
    def enter_pin(self):
        """Enter the PIN number to serarch for in the PIN search form
        and send a CR to get the form to search
        :returns: TODO
        """
        element = self.driver.find_element_by_class_name('dxeEditArea')
        element.send_keys(self.pin)  # onkey event here
        # send CR
        element.send_keys('\n')  # onkey event here
        return element
    def write_png(self):
        """Write a png file to disk
        :returns: png image
        """
        # wait a bit, brother ...
        time.sleep(5)
        fn = '%s_%s.png' % (self.pin, time.strftime('%Y-%m-%d-%H-%M'))
        fqn = os.path.join('/var/tmp/worblehat', fn)
        png = self.driver.get_screenshot_as_file(fqn)
        msg = 'Wrote image to %s' % (fqn)
        logger.info(msg)
        return png
    def doc_icon(self):
        """
        does not work yet ...
        """
        # click on document icon
        # XXX FIXME: use lxml parsing 2015/03/15  (james)
        wait = WebDriverWait(self.driver, 10)
        # doc_id = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'dxeHyperlink'))
        doc_icon = wait.until(EC.element_to_be_clickable((By.ID, doc_id)))
        # doc_icon = driver.find_element_by_id(doc_id)
        self.actionChains.click(doc_icon).perform()
    def _find_docgroup(self, elements):
        """
        find the group that contains
        the document icon we want
        """
        out = []
        for num, elem in enumerate(elements):
            if self.pin in elem.text:
                out = elements[num + 1]
                return out
        return out
if __name__ == '__main__':
    pin = Pin()
    pin()
