"""
File: text_parser.py
Author: James Whisnant
Email: jwhisnant@gmail.com
Github: https://github.com/jwhisnant
Description:  Text parsing of items from webdriver
"""


def parse_summary(text):
    """
    pp(foo.text.split('\n'))
    [u'Status: Active     Interest Owners Exist',
    u'PIN: 9748872017 Rate Code: 01 Tax Owner:',
    u'SubCode: Book/Page: 5844/122  ',
    u'Township: BINGHAM Date Recorded: 9/9/2014 Current Owner 1: INGOLD MELANIE L',
    u'Parcel Size: 11.13 Acres   Stamp Value: Current Owner 2: WHISNANT JAMES D III',
    u'      Account Type: Individual Address: 2337 WHITE CROSS RD',
    u'          Legal Description: 2A SAMUEL INGOLD P113/64   City: CHAPEL HILL',
    u'    State: NC Zip Code: 27516',
    u'Situs:',
    u'                           ']
    """


def parse_book(text):
    """
    pp(text.split('\n')
    [u'Book Page',
    u'Instrument Type',
    u'Date Recorded',
    u'Legal Description',
    u'Document',
    u'113/64 PLAT 8/25/2014 Lot:2A & 2 Sub:SAMUEL INGOLD Tp:BING Other:8-11-14',
    u'5837/381 SPLIT 8/25/2014 Lot:2 & 2A Sub:SAMUEL INGOLD Tp:BING Other:P113/64',
    u'5838/366 AGMT 8/27/2014 Other:NO USEABLE DESCRIPTION',
    u'5844/122 DEED 9/9/2014 Lot:2A Sub:SAMUEL INGOLD Tp:BING Other:P113/64',
    u'5882/233 R/W 12/16/2014 Ac:11.13',
    u'5907/478 D/T 2/13/2015 Lot:2A Sub:SAMUEL INGOLD Tp:BING Other:P113/64']
    """
